class AddUserToContacts < ActiveRecord::Migration
  def change
		change_table :contacts do |t|
			t.references :user
		end
  end
end
