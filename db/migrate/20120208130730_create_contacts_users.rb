class CreateContactsUsers < ActiveRecord::Migration
  def change
		create_table :contacts_users, :id => false do |t|
			t.references :user
			t.references :contact
		end
  end

end
