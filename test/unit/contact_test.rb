require 'test_helper'

class ContactTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

 test "should create a contact" do
	  c = Contact.new(:first_name => "Matthieu", :last_name => "Segret")
	  assert c.save, "Contact created"
	  c_fetched = Contact.find_by_last_name("Segret")

		assert_equal c_fetched.first_name, "Matthieu"
	  assert_equal c_fetched.last_name, "Segret"	  

	end
	
	test "should not create a contact without last_name" do
	  c = Contact.new(:first_name => "Toto")
	  assert !c.save
	  assert c.errors[:last_name].present? 
	end
	
	test "should update last_name" do
		c = contacts("Marion")
		assert c.update_attributes(:last_name => "Gogo"), "Marion last_name updated"
	end	
	
	test "should search contact with last_name" do
		contacts_fetched = Contact.search("Geoffroy")
		assert_equal contacts_fetched.length, 1
		c_fetched = contacts_fetched[0]
		assert_equal c_fetched.first_name, "Marion"
	  assert_equal c_fetched.last_name, "Geoffroy"	  		
	end

end
