class Contact < ActiveRecord::Base
	validates_presence_of :last_name
	validates_length_of :last_name, :minimum => 1
	
	has_and_belongs_to_many :users
	
	self.per_page = 3
	
	def self.search(last_name)
		Contact.find_all_by_last_name(last_name)
	end
	
	def as_json(opts={})
		{ :first_name => self.first_name, :last_name => self.last_name }
	end
	
end
